FROM amazon/aws-cli:latest
RUN yum -y install python3 \
    python3-pip \
    shadow-utils \
    util-linux
RUN yum update
RUN adduser maynard
#RUN usermod -aG wheel
